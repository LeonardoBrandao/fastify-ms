import { UserController } from '../controllers/user.controller';

export const userRoutes = (fastify, opts, next) => {
    fastify.get('/user', UserController.getAllUsers);
    fastify.post('/user', UserController.addUser);
    next();
};
