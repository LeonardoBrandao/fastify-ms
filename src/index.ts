import * as f from "fastify";
import * as mongoose from 'mongoose';
import { userRoutes } from './routes/user.routes';

const fastify: any = f({ logger: true });

mongoose.connect('mongodb://localhost:27017/fastify', {useNewUrlParser: true});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

fastify.register(userRoutes, {logLevel: 'debug'});

const start = async () => {
  try {
    await fastify.listen(3000, "0.0.0.0");
  } catch (err) {
    console.log(err);
    fastify.log.error(err);
    process.exit(1);
  }
};

process.on("uncaughtException", error => {
  console.error(error);
});
process.on("unhandledRejection", error => {
  console.error(error);
});

start();