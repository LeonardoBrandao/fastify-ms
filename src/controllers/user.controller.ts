import userModel from '../models/user.model';

export class UserController {
    static async getAllUsers(req, res) {
        const response = await userModel.find().exec();
        res.send(response);
    }

    static async addUser({body}, res) {
        const user = new userModel(body);
        const response = await user.save();
        res.send(response);
    }
}
