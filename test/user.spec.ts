import userModel from '../src/models/user.model';
import { UserController } from '../src/controllers/user.controller';

jest.mock('../src/models/user.model.ts');

describe('User Controller', () => {

  test('Should retrieve all users from db', () => {
    const query = [
      {
        _id: '5cb7eb2ba1f5543364b8a237',
        firstName: 'leo1',
        lastName: 'teste1',
        age: 23,
        __v: 0,
      },
    ];
    userModel.find.mockReturnValue(query);

    expect(UserController.getAllUsers()).toBe(query);
  });
});
